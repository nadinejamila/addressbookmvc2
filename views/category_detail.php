<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Category Info</title>
        <link rel="stylesheet" type="text/css" media="screen" 
              href="/DSS2/views/css/styles.css" />
    </head>
    <body>
        <div id="divWrapper">
            <form name="category_form" method="POST" 
                  action="/DSS2/category/<?php 
                    echo (isset($cat_id) ? 'update' : 'add') ?>" 
                  class="contact_form">
                <ul>
                    <li>
                        <h2>Add New Category Info</h2>
                        <!--<span class="required_notification">* Denotes Required Field</span>-->
                        <input type="hidden" name="cat_id" 
                               value="<?php echo (isset($cat_id) ? $cat_id : -1) ?>" />
                    </li>
                    <li>
                        <label for="cat_name">Category name: </label>
                        <input type="text" name="cat_name" 
                               value="<?php (isset($category)) && (print $category->getCatName()) ?>" 
                               placeholder="e.g. Programming Languages" />
                    </li>

                    <li>
                        <input type="submit" name="action" value="Save" />
                        <input type="reset" name="reset" value="Reset" />
                    </li>
                </ul>
            </form>
            <div class="nav-bar">
                <a href="/DSS2/category/">Back</a>
            </div>
        </div>
    </body>
</html>
