<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Categories</title>
        <base href="/DSS2/"/>
        <link rel="stylesheet" type="text/css" media="screen" 
              href="views/css/TableCSSCode.css" />
    </head>
    <body>
        <?php
            (isset($message)) && (print "<h2>$message</h2>");
        ?>
        <h1>Decision Support System</h1>
	<br/>
        <div class='CSSTableGenerator'>
            <table><tbody>
                <tr>
                    <th>Category</th>

                    <th> </th><th> </th><th> </th>
                </tr>
                <?php
                    foreach ($category_list as $category) {
                        echo ("<tr>\n<td>".$category->getCatName()."</td>\n");
                        echo ("<td><a href='category/view/".
                                    $category->getCatId()."'>Update</a></td>\n");
  			echo ("<td><a href='category/view/".
                                    $category->getCatId()."'>Show Options</a></td>\n");
                        echo "<td><form method='POST' ";
                        echo "action='category/delete'>\n";
                        echo "<input type='hidden' name='id' ";
                        echo ("value='".$category->getCatId()."' />\n");
                        echo "<input type='submit' name='action' ";
                        echo "class='btn-delete' value='Delete' /></form></td>\n<tr>\n";
                    }   
                ?>
            </tbody></table>
        </div>
        <div class="nav-bar">
            <a href="category/newCategory">Add another entry</a>
        </div>
    </body>
</html>
