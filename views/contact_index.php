<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Address Book</title>
        <link rel="stylesheet" type="text/css" media="screen" 
              href="/AddressBookMVC2/views/css/TableCSSCode.css" />
    </head>
    <body>
        <?php
            (isset($message)) && (print "<h2>$message</h2>");
        ?>
        <h2>Address Book</h2>
        <div class='CSSTableGenerator'>
            <table><tbody>
                <tr>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Phone no.</th>
                    <th> </th><th> </th>
                </tr>
                <?php
                    foreach ($contact_list as $contact) {
                        echo ("<tr>\n<td>".$contact->getFirstName()."</td>\n");
                        echo ("<td>".$contact->getLastName()."</td>\n");
                        echo ("<td>".$contact->getAddress()."</td>\n");
                        echo ("<td>".$contact->getPhone()."</td>\n");
                        echo ("<td>".$contact->getEmail()."</td>\n");
                        echo ("<td><a href='view/".
                                    $contact->getId()."'>Update</a></td>\n");
                        echo "<td><form method='POST' ";
                        echo "action='delete'>\n";
                        echo "<input type='hidden' name='id' ";
                        echo ("value='".$contact->getId()."' />\n");
                        echo "<input type='submit' name='action' ";
                        echo "class='btn-delete' value='Delete' /></form></td>\n<tr>\n";
                    }   
                ?>
            </tbody></table>
        </div>
        <div class="nav-bar">
            <a href="contact/newContact">Add another entry</a>
        </div>
    </body>
</html>
