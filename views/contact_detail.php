<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contact Info</title>
        <link rel="stylesheet" type="text/css" media="screen" 
              href="/AddressBookMVC2/views/css/styles.css" />
    </head>
    <body>
        <div id="divWrapper">
            <form name="contact_form" method="POST" 
                  action="/AddressBookMVC2/contact/<?php 
                    echo (isset($id) ? 'update' : 'add') ?>" 
                  class="contact_form">
                <ul>
                    <li>
                        <h2>Add New Contact Info</h2>
                        <!--<span class="required_notification">* Denotes Required Field</span>-->
                        <input type="hidden" name="id" 
                               value="<?php echo (isset($id) ? $id : -1) ?>" />
                    </li>
                    <li>
                        <label for="first_name">First name: </label>
                        <input type="text" name="first_name" 
                               value="<?php (isset($contact)) && (print $contact->getFirstName()) ?>" 
                               placeholder="Juan" />
                    </li>
                    <li>
                        <label for="last_name">Last name:</label>
                        <input type="text" name="last_name" 
                               value="<?php (isset($contact)) && (print $contact->getLastName()) ?>" 
                               placeholder="dela Cruz" />
                    </li>
                    <li>
                        <label for="address">Address: </label>
                        <textarea name="address" rows="5" cols="40" 
                                  placeholder="Quezon City" ><?php (isset($contact)) && 
                                  (print $contact->getAddress()) ?></textarea>
                    </li>
                    <li>
                        <label for="phone">Phone no.:</label>
                        <input type="text" name="phone" 
                               value="<?php (isset($contact)) && (print $contact->getPhone()) ?>" 
                               placeholder="0919 999-9999" />
                    </li>
                    <li>
                        <label for="email">Email address:</label>
                        <input type="text" name="email" 
                               value="<?php (isset($contact)) && (print $contact->getEmail()) ?>" 
                               placeholder="juan.dela.cruz@gmail.com" />  
                    </li>
                    <li>
                        <input type="submit" name="action" value="Save" />
                        <input type="reset" name="reset" value="Reset" />
                    </li>
                </ul>
            </form>
            <div class="nav-bar">
                <a href="/AddressBookMVC2/contact/">Back</a>
            </div>
        </div>
    </body>
</html>
