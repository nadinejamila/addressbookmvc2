<?php

class Option {
    private $opt_id;
    private $cat_name;
    
    public function __construct($opt_id, $opt_name, $total_weight, $cat_id) {
        $this->opt_id = $opt_id;
        $this->opt_name = $opt_name;
        $this->total_weight = $total_weight;
        $this->cat_id = $cat_id;
    }
    
    public function getOptId() {
        return $this->opt_id;
    }
    
    public function setOptId($opt_id) {
        $this->opt_id = $opt_id;
    }
    
    public function getOptName() {
        return $this->opt_name;
    }
    
    public function setOptName($opt_name) {
        $this->opt_name = $opt_name;
    }

    public function getTotalWeight() {
        return $this->total_weight;
    }
    
    public function setTotalWeight($total_weight) {
        $this->total_weight = $total_weight;
    }
    
    public function getCatId() {
        return $this->cat_id;
    }
    
    public function setCatId($cat_id) {
        $this->cat_id = $cat_id;
    }
    
}

?>
