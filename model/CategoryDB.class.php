<?php
require_once('includes/Dsn.php');


class CategoryDB {
    private static $categoryDB;
    
    private $dbConnection;
    
    private $hostname;
    private $username;
    private $password;
    private $dbName;
    
    protected function __construct() {
        $this->hostname = Dsn::HOSTNAME;
        $this->username = Dsn::USERNAME;
        $this->password = Dsn::PASSWORD;
        $this->dbName = Dsn::DBNAME;
        
        $this->dbConnection = new mysqli($this->hostname, $this->username, 
                $this->password, $this->dbName);
    }
    
    public static function getInstance() {
        if (!(self::$categoryDB)) {
            self::$categoryDB = new CategoryDB();
        }
        
        return self::$categoryDB;
    }
    
    private function __clone() {    
    }
    
    private function __wakeup() {
    }
    
    public function getHostName() {
        return $this->hostname;
    }
    
    public function setHostName($hostname) {
        $this->hostname = $hostname;
    }
    
    public function getUsername() {
        return $this->username;
    }
    
    public function setUsername($username) {
        $this->username = $username;
    }
    
    public function getPassword() {
        return $this->password;
    }
    
    public function setPassword($password) {
        $this->password = $password;
    }
    
    public function getDbName() {
        return $this->dbName;
    }
    
    public function setDbName($dbName) {
        $this->dbName = $dbName;
    }
    
    public function getByCatId($cat_id) {
        if ($this->dbConnection) {
            $stmt = $this->dbConnection->prepare("SELECT cat_id, cat_name FROM categories WHERE cat_id = ?");
            
            if ($stmt && $stmt->bind_param("i", $cat_id) && 
                $stmt->execute()) {
                $result = $stmt->get_result();
                $row = $result->fetch_assoc();

                $newCategory = new Category($row['cat_id'], 
                        $row['cat_name']);
		
                return $newCategory;
            }
        }
        
        return null;
    }
    
    public function getList() {
        if ($this->dbConnection) {
            $result = $this->dbConnection->query("SELECT cat_id, cat_name FROM categories");
            
            if ($result) {
                $categoryList = array();
        
                while ($row = $result->fetch_assoc()) {
                    $newCategory = new Category($row['cat_id'], 
                            $row['cat_name']);

                    $categoryList[] = $newCategory;
                }
                
                return $categoryList;
            }
        }
        
        return null;
    }
    

    public function add(Category $category) {
        if ($this->dbConnection) {
            $stmt = $this->dbConnection->prepare("INSERT INTO categories".
                    "(cat_name) ".
                    "VALUES(?)");
            
            if ($stmt) {
                $cat_name = $category->getCatName();
                
                $isBound = $stmt->bind_param("s", $cat_name);
                
                $result = ($isBound && $stmt->execute());
                
                return $result;
            }
        }
        
        return false;
    }
    
    public function update(Category $category) {
        if ($this->dbConnection) {
            $stmt = $this->dbConnection->prepare("UPDATE categories ".
                    "SET cat_name = ? WHERE cat_id = ?");
            
            if ($stmt) {
                $cat_name = $category->getCatName();
                $cat_id = $category->getCatId();
                
                $isBound = $stmt->bind_param("si", $cat_name, $cat_id);
                
                $result = ($isBound && $stmt->execute());
                
                return $result;
            }
        }
        
        return false;
    }
    

    public function delete($cat_id) {
        if ($this->dbConnection) {
            $stmt = $this->dbConnection->prepare("DELETE FROM categories WHERE cat_id = ?");
            
            if ($stmt && $stmt->bind_param("i", $cat_id)) {  
                $result = $stmt->execute();
                
                return $result;
            }
        }
        
        return false;
    }
    
    public function __destruct() {
        if ($this->dbConnection) {
            $this->dbConnection->close();
        }
    }
}

?>
