<?php
require_once('includes/Dsn.php');

/**
 * Manages and performs queries on the table  of contacts in the 
 * address book database
 *
 * @author Reggie
 */
class ContactDB {
    private static $contactDB;
    
    private $dbConnection;
    
    private $hostname;
    private $username;
    private $password;
    private $dbName;
    
    /**
     * Protected constructor to prevent creating a new instance of the
     * *ContactDB* via the `new` operator from outside of this class.
     * 
     * @param string $hostname
     * @param string $username
     * @param string $password
     * @param string $dbName
     * 
     */
    protected function __construct() {
        $this->hostname = Dsn::HOSTNAME;
        $this->username = Dsn::USERNAME;
        $this->password = Dsn::PASSWORD;
        $this->dbName = Dsn::DBNAME;
        
        $this->dbConnection = new mysqli($this->hostname, $this->username, 
                $this->password, $this->dbName);
    }
    
    /**
     * Returns the singleton instance of this class.
     *
     * @return ContactDB The singleton instance.
     */
    public static function getInstance() {
        if (!(self::$contactDB)) {
            self::$contactDB = new ContactDB();
        }
        
        return self::$contactDB;
    }
    
    /**
     * Private clone method to prevent cloning of the instance of the
     * *ContactDB* instance.
     *
     * @return void
     */
    private function __clone() {
        
    }
    
    /**
     * Private unserialize method to prevent unserializing of the *ContactDB*
     * instance.
     *
     * @return void
     */
    private function __wakeup()
    {
    }
    
    public function getHostName() {
        return $this->hostname;
    }
    
    public function setHostName($hostname) {
        $this->hostname = $hostname;
    }
    
    public function getUsername() {
        return $this->username;
    }
    
    public function setUsername($username) {
        $this->username = $username;
    }
    
    public function getPassword() {
        return $this->password;
    }
    
    public function setPassword($password) {
        $this->password = $password;
    }
    
    public function getDbName() {
        return $this->dbName;
    }
    
    public function setDbName($dbName) {
        $this->dbName = $dbName;
    }
    
    /**
     * Returns the contact with the given ID number
     * @param type $id
     * @return \Contact|null
     */
    public function getById($id) {
        if ($this->dbConnection) {
            $stmt = $this->dbConnection->prepare("SELECT id, first_name, ".
                "last_name, address, email, phone FROM address_book ".
                    "WHERE id = ?");
            
            if ($stmt && $stmt->bind_param("i", $id) && 
                $stmt->execute()) {
                $result = $stmt->get_result();
                $row = $result->fetch_assoc();

                $newContact = new Contact($row['id'], 
                        $row['first_name'], $row['last_name'], 
                        $row['address'], $row['phone'], 
                        $row['email']);

                return $newContact;
            }
        }
        
        return null;
    }
    
    /**
     * Returns the list of all the contacts in  the database
     * @return \Contact|null
     */
    public function getList() {
        if ($this->dbConnection) {
            $result = $this->dbConnection->query("SELECT id, first_name, ".
                "last_name, address, email, phone FROM address_book");
            
            if ($result) {
                $contactList = array();
        
                while ($row = $result->fetch_assoc()) {
                    $newContact = new Contact($row['id'], 
                            $row['first_name'], $row['last_name'], 
                            $row['address'], $row['phone'], 
                            $row['email']);

                    $contactList[] = $newContact;
                }
                
                return $contactList;
            }
        }
        
        return null;
    }
    
    /**
     * Adds a new contact into the database
     * @param Contact $contact
     * @return boolean True if the insertion is successful; false, otherwise
     */
    public function add(Contact $contact) {
        if ($this->dbConnection) {
            $stmt = $this->dbConnection->prepare("INSERT INTO address_book".
                    "(first_name, last_name, address, phone, email) ".
                    "VALUES(?, ?, ?, ?, ?)");
            
            if ($stmt) {
                $first_name = $contact->getFirstName();
                $last_name = $contact->getLastName();
                $address = $contact->getAddress();
                $email = $contact->getEmail();
                $phone = $contact->getPhone();
                
                $isBound = $stmt->bind_param("sssss", $first_name, 
                        $last_name, $address, $phone, $email);
                
                $result = ($isBound && $stmt->execute());
                
                return $result;
            }
        }
        
        return false;
    }
    
    /**
     * Updates the details of the given contact in the database
     * @param Contact $contact
     * @return boolean True if the update is successful; false, otherwise
     */
    public function update(Contact $contact) {
        if ($this->dbConnection) {
            $stmt = $this->dbConnection->prepare("UPDATE address_book ".
                    "SET first_name = ?, last_name = ?, address = ?, ".
                    "phone = ?, email = ? WHERE id = ?");
            
            if ($stmt) {
                $first_name = $contact->getFirstName();
                $last_name = $contact->getLastName();
                $address = $contact->getAddress();
                $email = $contact->getEmail();
                $phone = $contact->getPhone();
                $id = $contact->getId();
                
                $isBound = $stmt->bind_param("sssssi", $first_name, 
                        $last_name, $address, $phone, $email, $id);
                
                $result = ($isBound && $stmt->execute());
                
                return $result;
            }
        }
        
        return false;
    }
    
    /**
     * Deletes a contact from the database given the ID number
     * @param type $id
     * @return boolean True if the delete is successful; false, otherwise
     */
    public function delete($id) {
        if ($this->dbConnection) {
            $stmt = $this->dbConnection->prepare("DELETE FROM address_book ".
                    "WHERE id = ?");
            
            if ($stmt && $stmt->bind_param("i", $id)) {  
                $result = $stmt->execute();
                
                return $result;
            }
        }
        
        return false;
    }
    
    public function __destruct() {
        if ($this->dbConnection) {
            $this->dbConnection->close();
        }
    }
}

?>
