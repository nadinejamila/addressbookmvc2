<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 */
class User {
    private $userId;
    
    private $password;
    
    private $firstName;
    
    private $lastName;
    
    private $contactNo;
    
    private $emailAdd;
    
    private $street;
    
    private $landmarks;
    
    private $city;
    
    function __construct($userId, $password, $firstName, $lastName, $contactNo, $emailAdd, $street, $landmarks, $city) {
        $this->userId = $userId;
        $this->password = $password;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->contactNo = $contactNo;
        $this->emailAdd = $emailAdd;
        $this->street = $street;
        $this->landmarks = $landmarks;
        $this->city = $city;
    }

    public function getUserId() {
        return $this->userId;
    }

    public function setUserId($userId) {
        $this->userId = $userId;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function getFirstName() {
        return $this->firstName;
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    public function getContactNo() {
        return $this->contactNo;
    }

    public function setContactNo($contactNo) {
        $this->contactNo = $contactNo;
    }

    public function getEmailAdd() {
        return $this->emailAdd;
    }

    public function setEmailAdd($emailAdd) {
        $this->emailAdd = $emailAdd;
    }

    public function getStreet() {
        return $this->street;
    }

    public function setStreet($street) {
        $this->street = $street;
    }

    public function getLandmarks() {
        return $this->landmarks;
    }

    public function setLandmarks($landmarks) {
        $this->landmarks = $landmarks;
    }

    public function getCity() {
        return $this->city;
    }

    public function setCity($city) {
        $this->city = $city;
    }
}

?>
