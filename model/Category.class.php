<?php

class Category {
    private $cat_id;
    private $cat_name;
    
    public function __construct($cat_id, $cat_name) {
        $this->cat_id = $cat_id;
        $this->cat_name = $cat_name;
    }
    
    public function getCatId() {
        return $this->cat_id;
    }
    
    public function setCatId($cat_id) {
        $this->cat_id = $cat_id;
    }
    
    public function getCatName() {
        return $this->cat_name;
    }
    
    public function setCatName($cat_name) {
        $this->cat_name = $cat_name;
    }
    
}

?>
