<?php

class categoryController extends baseController {
    
    /**
     * Displays the list of categories available
     */
    public function index() {
        $this->registry->template->category_list = $this->registry->category_db->getList();
        $this->registry->template->show('category_index');
    }
    
    /**
     * Displays a form for adding a new category
     */
    public function newCategory() {
        $this->registry->template->show('category_detail');
    }
    
    /**
     * Displays a form with the category details for editing
     */
    public function view() {
        $cat_id = $this->registry->template->cat_id;
        $category = $this->registry->category_db->getByCatId($cat_id);
        $this->registry->template->category = $category;
        
        $this->registry->template->show('category_detail');
    }
    
    public function add() {
        $newCategory = new Category(-1, $this->registry->template->cat_name);
        $is_added = $this->registry->category_db->add($newCategory);
        $this->registry->template->message = ($is_added) ? 
                Resource::MSG_ADD_SUCCESS : Resource::MSG_ADD_ERROR;
        $this->registry->template->category_list = $this->registry->category_db->getList();
        
        $this->registry->template->show('category_index');
    }
    
    public function update() {
        $category = new Category($this->registry->template->cat_id, 
                $this->registry->template->cat_name);
        $is_updated = $this->registry->category_db->update($category);
        $this->registry->template->message = ($is_updated) ? 
                Resource::MSG_UPDATE_SUCCESS : Resource::MSG_UPDATE_ERROR;
        $this->registry->template->category_list = $this->registry->category_db->getList();
        
        $this->registry->template->show('category_index');
    }
    

    public function delete() {
        $cat_id = $this->registry->template->cat_id;
        $is_deleted = $this->registry->category_db->delete($cat_id);
        $this->registry->template->message = ($is_deleted) ? 
                Resource::MSG_DELETE_SUCCESS : Resource::MSG_DELETE_ERROR;
        $this->registry->template->category_list = $this->registry->category_db->getList();
        
        $this->registry->template->show('category_index');
    }
}

?>
