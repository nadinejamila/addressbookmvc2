<?php

/**
 * Description of contactController
 *
 * @author Reggie
 */
class contactController extends baseController {
    
    /**
     * Displays the list of contacts available within the address book
     */
    public function index() {
        $this->registry->template->contact_list = $this->registry->contact_db->getList();
        $this->registry->template->show('contact_index');
    }
    
    /**
     * Displays a form for adding an entry into the address book
     */
    public function newContact() {
        $this->registry->template->show('contact_detail');
    }
    
    /**
     * Displays a form with the contact details for editing
     */
    public function view() {
        $contact_id = $this->registry->template->id;
        $contact = $this->registry->contact_db->getById($contact_id);
        $this->registry->template->contact = $contact;
        
        $this->registry->template->show('contact_detail');
    }
    
    /**
     * Adds an entry into the address book
     */
    public function add() {
        $newContact = new Contact(-1, $this->registry->template->first_name, 
                $this->registry->template->last_name, 
                $this->registry->template->address, 
                $this->registry->template->phone, 
                $this->registry->template->email);
        $is_added = $this->registry->contact_db->add($newContact);
        $this->registry->template->message = ($is_added) ? 
                Resource::MSG_ADD_SUCCESS : Resource::MSG_ADD_ERROR;
        $this->registry->template->contact_list = $this->registry->contact_db->getList();
        
        $this->registry->template->show('contact_index');
    }
    
    /**
     * Updates the details of an entry in the address book
     */
    public function update() {
        $contact = new Contact($this->registry->template->id, 
                $this->registry->template->first_name, 
                $this->registry->template->last_name, 
                $this->registry->template->address, 
                $this->registry->template->phone, 
                $this->registry->template->email);
        $is_updated = $this->registry->contact_db->update($contact);
        $this->registry->template->message = ($is_updated) ? 
                Resource::MSG_UPDATE_SUCCESS : Resource::MSG_UPDATE_ERROR;
        $this->registry->template->contact_list = $this->registry->contact_db->getList();
        
        $this->registry->template->show('contact_index');
    }
    
    /**
     * Deletes an entry from the address book
     */
    public function delete() {
        $contact_id = $this->registry->template->id;
        $is_deleted = $this->registry->contact_db->delete($contact_id);
        $this->registry->template->message = ($is_deleted) ? 
                Resource::MSG_DELETE_SUCCESS : Resource::MSG_DELETE_ERROR;
        $this->registry->template->contact_list = $this->registry->contact_db->getList();
        
        $this->registry->template->show('contact_index');
    }
}

?>
