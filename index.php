<?php
 /*** error reporting on ***/
 error_reporting(E_ALL);
 /*** define the site path constant ***/
 $site_path = realpath(dirname(__FILE__));
 define ('__SITE_PATH', $site_path);
 /*** include the init.php file ***/
 include 'includes/init.php';
 /*** a new registry object ***/
 $registry = new registry;
 /*** load the router ***/
 $registry->router = new Router($registry);
 /** load the database ***/
 $registry->category_db = CategoryDB::getInstance();
 /*** load the templating engine ***/
 $registry->template = new Template($registry);

 /*** set the path to the controllers directory ***/
 $registry->router->setPath(__SITE_PATH . '/controller');
 /*** let the controller handle the request ***/
 $registry->router->loader();

?>
