<?php

class Router {
 /*
 * @the registry
 */
 private $registry;

 /*
 * @the controller path
 */
 private $path;

 private $args = array();

 public $file;

 public $controller;

 public $action;

 function __construct($registry) {
        $this->registry = $registry;
 }
 
 /**
 *
 * @set controller directory path
 *
 * @param string $path
 *
 * @return void
 *
 */
 function setPath($path) {

        /*** check if path i sa directory ***/
        if (!is_dir($path))
        {
                throw new Exception ('Invalid controller path: `' . $path . '`');
        }
        /*** set the path ***/
        $this->path = $path;
}

 /**
 *
 * Determines the controller for the request given the URL
 *
 * @access private
 *
 * @return void
 *
 */
private function setController() {

        /*** get the route from the url ***/
        $route = (empty($_GET['rt'])) ? '' : $_GET['rt'];

        if (empty($route))
        {
                $route = 'index';
        }
        else
        {
                /*** get the parts of the route ***/
                $parts = explode('/', $route);
                $this->controller = $parts[0];
                if (isset($parts[1])) {
                    $this->action = $parts[1];
                }
                if (isset($parts[2])) {
                    $this->registry->template->cat_id = $parts[2];
                }
        }

        if (empty($this->controller))
        {
                $this->controller = 'index';
        }

        /*** Get action ***/
        if (empty($this->action))
        {
                $this->action = 'index';
        }

        /*** set the file path ***/
        $this->file = $this->path .'/'. $this->controller . 'Controller.php';
}

/**
 * 
 * Loads the request variables into the registry template
 * 
 * @access private
 * 
 * @return void
 * 
 */
private function setRequestVariables() {
    $this->registry->request_method = $_SERVER['REQUEST_METHOD'];
    
    foreach ($_GET as $key => $value) {
        $this->registry->template->$key = $value;
    }
    
    foreach ($_POST as $key => $value) {
        $this->registry->template->$key = $value;
    }
}

 /**
 *
 * @load the controller
 *
 * @access public
 *
 * @return void
 *
 */
 public function loader()
 {
        /*** check the route ***/
        $this->setController();
        
        /*** load the request variables ***/
        $this->setRequestVariables();
        
        /*** if the file is not there, die ***/
        if (!is_readable($this->file))
        {
                die ("$this->file: 404 Not Found");
        }

        /*** include the controller ***/
        include $this->file;

        /*** a new controller class instance ***/
        $class = $this->controller . 'Controller';
        $controller = new $class($this->registry);

        /*** check if the action is callable ***/
        if (!is_callable(array($controller, $this->action)))
        {
                $action = 'index';
        }
        else
        {
                $action = $this->action;
        }
        /*** run the action ***/
        $controller->$action();
 }
 
}

?>
