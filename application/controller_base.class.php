<?php

abstract class baseController {

/*
 * @registry object
 */
protected $registry;

public function __construct($registry) {
        $this->registry = $registry;
}

/**
 * @all controllers must contain an index method
 */
abstract function index();
}

?>