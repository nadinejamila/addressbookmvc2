<?php

class Resource {
    /* CHANGE THIS */
    const MSG_ADD_SUCCESS = 'You have successfully added a category in your Decision Support System';
    
    const MSG_ADD_ERROR = 'An error has occurred while adding another category in your Decision Support System.';
    
    const MSG_UPDATE_SUCCESS = 'You have successfully updated a category in your Decision Support System.';
    
    const MSG_UPDATE_ERROR = 'An error has occurred while updating a category in your Decision Support System.';
    
    const MSG_DELETE_SUCCESS = 'You have successfully deleted a category from your Decision Support System.';
    
    const MSG_DELETE_ERROR = 'An error has occurred while deleting a category from your Decision Support System.';
}

?>
